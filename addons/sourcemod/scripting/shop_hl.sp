#pragma semicolon 1

#include <sourcemod>
#include <sdktools>
#include <cstrike>
#include <sdkhooks>
#include <shop>
#include <t2lrcfg>
#include <sm_jail_redie>
#pragma newdecls required
#define VERSION "1.0.4"

public Plugin myinfo = 
{
	name		= "HL",
	author		= "NoTiCE & ShaRen",
	description = "HL",
	version		= VERSION,
	url			= "Servers-Info.Ru"
};

Handle g_hCvar_HealDistance		= INVALID_HANDLE;
Handle g_hCvar_SearchDistance	= INVALID_HANDLE;
Handle g_hCvar_TerHeal			= INVALID_HANDLE;
Handle g_hCvar_CtHeal			= INVALID_HANDLE;

Handle g_hHealTimers[MAXPLAYERS+1] = {INVALID_HANDLE, ...};
int g_iHealTarget[MAXPLAYERS+1]	 = {-1, ...};
int g_iEffectLight[MAXPLAYERS+1] = {-1, ...};
int m_hMyWeapons;						// for detecting taser
float g_fFloodTime[MAXPLAYERS+1];

public void OnPluginStart()
{
	g_hCvar_HealDistance	= CreateConVar("sm_hl_heal_distance", "150", "Max healing distance", FCVAR_NOTIFY, true, 1.0);
	g_hCvar_SearchDistance	= CreateConVar("sm_hl_search_distance", "500", "Max search distance", FCVAR_NOTIFY, true, 1.0);
	g_hCvar_TerHeal			= CreateConVar("sm_hl_ter_heal", "10", "Recoverable amount of the health for Ter", FCVAR_NOTIFY, true, 1.0);
	g_hCvar_CtHeal			= CreateConVar("sm_hl_cter_heal", "3", "Recoverable amount of the health for Ct", FCVAR_NOTIFY, true, 1.0);
	AutoExecConfig(true, "hl");
	
	HookEvent("round_start", Event_RoundStart, EventHookMode_PostNoCopy);
	HookEvent("player_death", Event_PlayerDeath);

	m_hMyWeapons = FindSendPropInfo("CBasePlayer", "m_hMyWeapons");
	
	if(m_hMyWeapons == -1) {
		char Error[128];
		FormatEx(Error, sizeof(Error), "FATAL ERROR m_hMyWeapons [%d]. Please contact the author.", m_hMyWeapons);
		SetFailState(Error);
	}
}

public void OnMapStart()
{
	for(int i = 1; i <= MaxClients; i++) {
		g_hHealTimers[i] = INVALID_HANDLE;
		g_iHealTarget[i] = -1;
		g_iEffectLight[i] = -1;
	}
}

public void Event_RoundStart(Handle event, const char[] name, bool dontBroadcast)
{
	for(int i = 1; i <= MaxClients; i++) {
		if(g_hHealTimers[i] != INVALID_HANDLE) {
			KillTimer(g_hHealTimers[i]);
			g_hHealTimers[i] = INVALID_HANDLE;
		}
		g_iHealTarget[i] = -1;
		g_iEffectLight[i] = -1;
	}
}

public void Event_PlayerDeath(Handle event, const char[] name, bool dontBroadcast)
{
	int client = GetClientOfUserId(GetEventInt(event, "userid"));
	if(g_hHealTimers[client] != INVALID_HANDLE) {
		KillTimer(g_hHealTimers[client]);
		g_hHealTimers[client] = INVALID_HANDLE;
		KillEffect(client);
	}
	
	if(g_iHealTarget[client] != -1 && g_hHealTimers[g_iHealTarget[client]] != INVALID_HANDLE) {
		KillTimer(g_hHealTimers[g_iHealTarget[client]]);
		g_hHealTimers[g_iHealTarget[client]] = INVALID_HANDLE;
		if(IsClientInGame(g_iHealTarget[client]) && IsPlayerAlive(g_iHealTarget[client]) && !IsPlayerGhost(g_iHealTarget[client]))
			PrintHintText(g_iHealTarget[client], "Игрок, который вас лечил, умер. Лечение прекращено");
	}
	
	g_iHealTarget[client] = -1;
	
	int healer = FindHealer(client);
	if(IsClientIndex(healer) && IsClientInGame(healer) && IsPlayerAlive(healer) && !IsPlayerGhost(healer)) {
		PrintHintText(healer, "Вы больше не лечите игрока %N", client);
		g_iHealTarget[healer] = -1;
	}
}

public Action OnPlayerRunCmd(int client, int &buttons, int &impulse, float vel[3], float angles[3], int &weapon)
{
	static iButtons[MAXPLAYERS+1] = {0, ...};
	
	if(!IsClientInGame(client) || IsFakeClient(client))
		return Plugin_Continue;
	
	if((buttons & IN_USE) && !(iButtons[client] & IN_USE)) {
		int target = GetClientAimTarget2(client, true);
		if(IsClientIndex(target) && IsClientInGame(target) && IsPlayerAlive(target) && !IsPlayerGhost(target)) {
			if(GetClientTeam(client) == 3 && IsPlayerAlive(client) && !IsPlayerGhost(client)) {
				int iHealth = GetEntProp(target, Prop_Send, "m_iHealth");
				float fTargetPos[3], fClientPos[3];
				GetClientAbsOrigin(target, fTargetPos);
				GetClientAbsOrigin(client, fClientPos);
				float fDistance = GetVectorDistance(fTargetPos, fClientPos);
				float fCvar_SearchDistance = GetConVarFloat(g_hCvar_SearchDistance);
				if (GetClientTeam(target) == 2) {
					if(fDistance < fCvar_SearchDistance) {
						if(GetPlayerWeaponSlot(target, 0) > 0) {
							PrintToChat(client, "У игрока %N есть основное оружие", target);
							ClientCommand(client, "play buttons/blip1.wav");
							if (GetGameTime() - g_fFloodTime[target] > 5.0) {
								g_fFloodTime[target] = GetGameTime();
								PrintToChat(target, "%N нашел у тебя основное оружие", client);
								ClientCommand(target, "play buttons/blip1.wav");
							}
						}
						if(GetPlayerWeaponSlot(target, 1) > 0) {
							PrintToChat(client, "У игрока %N есть пистолет", target);
							ClientCommand(client, "play buttons/blip1.wav");
							if (GetGameTime() - g_fFloodTime[target] > 5.0) {
								g_fFloodTime[target] = GetGameTime();
								PrintToChat(target, "%N нашел у тебя пистолет", client);
								ClientCommand(target, "play buttons/blip1.wav");
							}
						}
						if(GetPlayerWeaponSlot(target, 3) > 0) {
							PrintToChat(client, "У игрока %N есть граната", target);
							ClientCommand(client, "play buttons/blip1.wav");
							if (GetGameTime() - g_fFloodTime[target] > 5.0) {
								g_fFloodTime[target] = GetGameTime();
								PrintToChat(target, "%N нашел у тебя гранату", client);
								ClientCommand(target, "play buttons/blip1.wav");
							}
						}
						// for detecting taser
						for (int j=0, ent=0; j<128; j+=4) {
							ent = GetEntDataEnt2(target, m_hMyWeapons + j);
							if (ent>0) {
								char sWeapon[64];
								GetEntityClassname(ent, sWeapon, sizeof(sWeapon));
								if (StrEqual(sWeapon, "weapon_taser")) {
									PrintToChat(client, "У игрока %N есть Taser", target);
									ClientCommand(client, "play buttons/blip1.wav");
									if (GetGameTime() - g_fFloodTime[target] > 5.0) {
										g_fFloodTime[target] = GetGameTime();
										PrintToChat(target, "%N нашел у тебя Taser", client);
										ClientCommand(target, "play buttons/blip1.wav");
									}
									break;
								}
							}
						}
					} else PrintToChat(client, "Для обыска %N ты в %.1f раз дальше положенного", target, fDistance/fCvar_SearchDistance);
				}
				float fCvar_HealDistance = GetConVarFloat(g_hCvar_HealDistance);
				if(fDistance <= (fCvar_HealDistance)) {
					if(g_hHealTimers[target] == INVALID_HANDLE) {
						if(iHealth >= 100 || IsLrActivated())
							PrintHintText(client, "%N [%i %%]", target, iHealth);
						else {
							if(g_iHealTarget[client] != -1 && g_hHealTimers[g_iHealTarget[client]] != INVALID_HANDLE) {
								KillTimer(g_hHealTimers[g_iHealTarget[client]]);
								g_hHealTimers[g_iHealTarget[client]] = INVALID_HANDLE;
								KillEffect(g_iHealTarget[client]);
							}
							
							g_iHealTarget[client] = target;
							g_hHealTimers[target] = CreateTimer(0.7, Timer_Heal, target, TIMER_FLAG_NO_MAPCHANGE|TIMER_REPEAT);
							CreateEffect(target);
							PrintHintText(target, "Вас лечит %N", client);
							PrintHintText(client, "%N [%i %%]", target, iHealth);
						}
					} else PrintHintText(client, "%N [%i %%] уже лечится", target, iHealth);
				} else PrintHintText(client, "%N [%i %%]", target, iHealth);
			} else PrintHintText(client, "%N", target);
		}
	}
	
	iButtons[client] = buttons;
	
	return Plugin_Continue;
}

public Action Timer_Heal(Handle timer, any client)
{
	if(!IsClientInGame(client) || !IsPlayerAlive(client) || IsPlayerGhost(client)) {
		int healer = FindHealer(client);
		if(healer != -1)
			g_iHealTarget[healer] = -1;
		
		g_hHealTimers[client] = INVALID_HANDLE;
		return Plugin_Stop;
	}
	
	int healer = FindHealer(client);
	int iHealth = GetEntProp(client, Prop_Send, "m_iHealth");
	if(IsClientIndex(healer) && IsClientInGame(healer) && IsPlayerAlive(healer) && !IsPlayerGhost(healer)) {
		float fHealerPos[3], fClientPos[3];
		GetClientAbsOrigin(healer, fHealerPos);
		GetClientAbsOrigin(client, fClientPos);
		if(GetVectorDistance(fHealerPos, fClientPos) > GetConVarFloat(g_hCvar_HealDistance)*1.8) {
			g_iHealTarget[healer] = -1;
			g_hHealTimers[client] = INVALID_HANDLE;
			KillEffect(client);
			PrintHintText(client, "Вы слишко далеко от %N", healer);
			PrintHintText(healer, "Вы слишко далеко от %N [%i %%]", client, iHealth);
			
			return Plugin_Stop;
		}
	} else {
		g_hHealTimers[client] = INVALID_HANDLE;
		KillEffect(client);
		
		return Plugin_Stop;
	}
	
	
	if(iHealth >= 100) {
		g_iHealTarget[healer] = -1;
		g_hHealTimers[client] = INVALID_HANDLE;
		KillEffect(client);
		PrintHintText(client, "Вы здоровы");
		PrintHintText(healer, "%N [%i %%]", client, iHealth);
		
		return Plugin_Stop;
	}
	
	switch(GetClientTeam(client)) {
		case CS_TEAM_T:
			iHealth += GetConVarInt(g_hCvar_TerHeal);
		case CS_TEAM_CT:
			iHealth += GetConVarInt(g_hCvar_CtHeal);
	}
	
	if(iHealth > 100)
		iHealth = 100;
	
	PrintHintText(healer, "%N [%i %%]", client, iHealth);
	SetEntProp(client, Prop_Send, "m_iHealth", iHealth);
	if(Shop_GetClientCredits(healer) > 0)
		Shop_TakeClientCredits(healer, 1, 0);
	
	return Plugin_Continue;
}

int GetClientAimTarget2(int client, bool only_clients = true)
{
	float eyeloc[3], ang[3];
	GetClientEyePosition(client, eyeloc);
	GetClientEyeAngles(client, ang);
	TR_TraceRayFilter(eyeloc, ang, MASK_SOLID, RayType_Infinite, TRFilter_AimTarget, client);
	int entity = TR_GetEntityIndex();
	if((only_clients && entity >= 1 && entity <= MaxClients) || (entity > 0) )
		return entity;
	return -1;
}

public bool TRFilter_AimTarget(int entity, int mask, any client)
{
	return (entity == client) ? false:true;
}

bool IsClientIndex(int index)
{
	return (index > 0) && (index <= MaxClients);
}

int FindHealer(int target)
{
	int healer = -1;
	for(int client = 1; client <= MaxClients; client++)
		if(g_iHealTarget[client] == target) {
			healer = client;
			break;
		}
	return healer;
}

void CreateEffect(int client)
{
	char Name[32]; float Origin[3];
	GetClientAbsOrigin(client, Origin);
	Origin[2] += 30.0;
	Format(Name, sizeof(Name), "lamp_%d", client);
	
	DispatchKeyValue(client, "targetname", Name);
	
	int Light = CreateEntityByName("light_dynamic");
	
	char LightName[32];
	Format(LightName, sizeof(LightName), "light_%d", Light);
	
	DispatchKeyValue(Light, "targetname", LightName);
	
	DispatchKeyValue(Light, "classname", "my_light");
	DispatchKeyValue(Light, "_light", "0 255 0");
	DispatchKeyValue(Light, "distance", "100");
	DispatchKeyValue(Light, "spotlight_radius", "80");
	DispatchKeyValue(Light, "brightness", "1");
	DispatchKeyValue(Light, "style", "0");
	
	char SpawnOrg[50];
	Format(SpawnOrg, sizeof(SpawnOrg), "%f %f %f", Origin[0], Origin[1], Origin[2]);
	
	DispatchKeyValue(Light, "origin", SpawnOrg);
	DispatchSpawn(Light);
	
	SetEntPropEnt(Light, Prop_Send, "m_hOwnerEntity", client);
	
	AcceptEntityInput(Light, "TurnOn", client);
	SetVariantString(Name);
	AcceptEntityInput(Light, "SetParent");
	
	SDKUnhook(Light, SDKHook_SetTransmit, Hook_SetTransmit);
	
	g_iEffectLight[client] = Light;
}

public void OnEntityDestroyed(int entity)
{
	if(entity > 0 && IsValidEdict(entity)) {
		char classname[64];
		GetEdictClassname(entity, classname, sizeof(classname));
		if(StrEqual(classname, "my_light", false))
			SDKUnhook(entity, SDKHook_SetTransmit, Hook_SetTransmit);
	}
}

public Action Hook_SetTransmit(int entity, int client)
{
	if(entity > 0 && IsValidEdict(entity)) {
		int owner = GetEntPropEnt(entity, Prop_Send, "m_hOwnerEntity");
		if(IsClientIndex(owner) && IsClientInGame(owner))
			return Plugin_Handled;
	}
	
	return Plugin_Continue;
}

void KillEffect(int client)
{
	if(g_iEffectLight[client] > MaxClients && IsValidEntity(g_iEffectLight[client])) {
		AcceptEntityInput(g_iEffectLight[client], "Kill");
		g_iEffectLight[client] = -1;
	}
}